from setuptools import setup

setup(name='openvpn_tray',
      version='0.1',
      description='OpenVPN Tray',
      author='BL',
      packages=['openvpn_tray'],
      install_requires=['pycairo', 'PyGObject'],
      entry_points={
          'console_scripts': ['ovpntray=openvpn_tray.openvpn_tray:run']
      },
      include_package_data=True,
      python_requires='>=3.6, < 3.7')
