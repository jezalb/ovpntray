import gi

gi.require_version("Gtk", "3.0")
gi.require_version("AppIndicator3", "0.1")
from gi.repository import Gtk as gtk, AppIndicator3 as appindicator
import subprocess
import argparse
from pkg_resources import resource_filename


class OpenVPNTray:

    def __init__(self):
        self.process = None

    def show_tray(self):
        # indicator = appindicator.Indicator.new_with_path(id='openvpn-tray',
        #                                                  icon_name='vpn',
        #                                                  category=appindicator.IndicatorCategory.APPLICATION_STATUS,
        #                                                  icon_theme_path='./icons')
        # indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
        # indicator.set_title('openvpn-tray')

        icon_path = resource_filename('openvpn_tray', 'icons/vpn.svg')
        status_icon = gtk.StatusIcon.new_from_file(filename=icon_path)
        status_icon.set_title('openvpn-tray')
        status_icon.set_name('openvpn-tray')
        status_icon.set_visible(True)
        status_icon.connect('popup-menu', self.right_click_event)

        gtk.main()

    def openvpn(self, ovpn_file_path):
        return subprocess.Popen(['pkexec', 'openvpn', '--config', ovpn_file_path])

    def parse_args(self):
        parser = argparse.ArgumentParser(description='Starts VPN connection')
        parser.add_argument('key_file', action='store', help='Path to ovpn file')
        return parser.parse_args()

    def right_click_event(self, status_icon, button, time):
        menu = gtk.Menu()

        quit_item = gtk.MenuItem()
        quit_item.set_label("Quit")

        quit_item.connect("activate", self.quit)

        menu.append(quit_item)

        menu.show_all()
        #
        # def pos(menu, icon):
        #     return (Gtk.StatusIcon.position_menu(menu, icon))

        menu.popup(None, None, None, status_icon, button, time)

    def quit(self, obj):
        if self.process is not None:
            # pid = os.getpid()
            p = subprocess.run(['pkexec', 'kill', str(self.process.pid)])
            if p.returncode == 0:
                gtk.main_quit()
            # print(self.process.pid)
            # subprocess.check_call(['kill', str(self.process.pid)])
            # os.waitpid(self.process.pid, 0)
        else:
            gtk.main_quit()

    def main(self):
        args = self.parse_args()
        self.process = self.openvpn(args.key_file)
        self.show_tray()

        # current_process = psutil.Process()
        # children = current_process.children(recursive=True)
        # for child in children:
        #     print('Child pid is {}'.format(child.pid))


def run():
    OpenVPNTray().main()


if __name__ == '__main__':
    run()
